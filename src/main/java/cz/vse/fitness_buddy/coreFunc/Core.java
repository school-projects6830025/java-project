package cz.vse.fitness_buddy.coreFunc;

import java.sql.ResultSet;
import java.sql.SQLException;

public class Core {
    /**
     * instance databaze, se kterou aplikace komunikuje pres internet.
     * */
    static DatabazeRelay db;
    static {
        try {
            db = new DatabazeRelay("ip", "user", "password", "dbname");
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * promenne, ktere vypovidaji o stavu aplikace
     */
    private static boolean uzivatelPrihlasen;
    private static int userID;
    private static int naposledZvolenyCvikId;
    private static boolean jeZvolenyCvikOblibeny;
    public Core () {}
    /**
     * Sekce funkci na dotazovani se a zmeny v databazi.
     * */
    public static int vlozUzivatele(String jmeno, String email, String heslo) throws SQLException {
        String insertQuery = "insert into registrovany_uzivatel (uzivatelske_jmeno, email, heslo) values ('" + jmeno + "', '" + email + "', '" + heslo + "')";
        userID = db.vlozUzivatele(insertQuery);
        if(userID >= 0) uzivatelPrihlasen = true;
        return userID;
    }
    public static boolean existujeZaznam(String jmenoTabulky, String jmenoSloupce, String hodnota) throws SQLException {
        return db.existujeZaznam(jmenoTabulky,jmenoSloupce,hodnota);
    }
    public static String vratHodnotu(String jmenoTabulky, String jmenoSloupceRidici, String jmenoSloupceZiskany, String hodnota) throws SQLException {
        return db.vratHodnotu(jmenoTabulky, jmenoSloupceRidici, jmenoSloupceZiskany, hodnota);
    }
    public static void upravHodnotu(String jmenoTabulky, String jmenoSloupceRidici, String hodnota, String jmenoSloupceKeZmene, String hodnotaKUlozeni) throws SQLException {
        db.upravHodnotu(jmenoTabulky, jmenoSloupceRidici, hodnota, jmenoSloupceKeZmene, hodnotaKUlozeni);
    }
    public static String vratHodnotuPodle2Sloupcu(String jmenoTabulky, String jmenoSloupceRidici1, String hodnota1, String jmenoSloupceRidici2, String hodnota2, String jmenoSloupceZiskany) throws SQLException {
        return db.vratHodnotuPodle2Sloupcu(jmenoTabulky,jmenoSloupceRidici1,hodnota1,jmenoSloupceRidici2,hodnota2,jmenoSloupceZiskany);
    }
    public static void upravHodnotuPodle2Sloupcu(String jmenoTabulky, String jmenoSloupceRidici1, String jmenoSloupceRidici2, String jmenoSloupceKeZmene, String hodnotaKUlozeni) throws SQLException {
        db.upravHodnotuPodle2Sloupcu(jmenoTabulky, jmenoSloupceRidici1, jmenoSloupceRidici2, jmenoSloupceKeZmene, hodnotaKUlozeni);
    }
    public static void upravHodnotuPodle2Sloupcu(String jmenoTabulky, String jmenoSloupceRidici1, String hodnota1, String jmenoSloupceRidici2, String hodnota2, String jmenoSloupceKeZmene, String hodnotaKUlozeni) throws SQLException {
        db.upravHodnotuPodle2Sloupcu(jmenoTabulky,jmenoSloupceRidici1,hodnota1,jmenoSloupceRidici2,hodnota2,jmenoSloupceKeZmene,hodnotaKUlozeni);
    }
    public static ResultSet vratTabulku1Sloupec (String jmenoTabulky, String jmenoSloupce, boolean pouzitPodminku, String podminka) throws SQLException {
        return db.vratTabulku1Sloupec(jmenoTabulky,jmenoSloupce,pouzitPodminku,podminka);
    }
    public static ResultSet vratTabulku2Sloupce (String jmenoTabulky, String jmenoSloupce1, String jmenoSloupce2, boolean pouzitPodminku, String podminka) throws SQLException {
        return db.vratTabulku2Sloupce(jmenoTabulky, jmenoSloupce1, jmenoSloupce2, pouzitPodminku, podminka);
    }
    public static ResultSet vratRelaceCvikKategorie() throws SQLException {
        return db.vratRelaceCvikKategorie();
    }
    public static int vratPocetRadku (String jmenoTabulky) throws SQLException {
        return db.vratPocetRadku(jmenoTabulky);
    }
    public static int vratPocetRadku (String jmenoTabulky, boolean pouzitPodminku, String podminka) throws SQLException {
        return db.vratPocetRadku(jmenoTabulky,pouzitPodminku,podminka);
    }
    public static void vytvorOblibenyCvikZaznamy (int id) throws SQLException {
        db.vytvorOblibenyCvikZaznamy(id);
    }

    /**
     * gettery, settery stavovych promennych
     * @return
     */
    public static boolean isUzivatelPrihlasen() {
        return uzivatelPrihlasen;
    }
    public static void setUzivatelPrihlasen(boolean prihl) {
        uzivatelPrihlasen = prihl;
    }
    public static int getUserID () {
        return userID;
    }
    public static void setUserID (int id) {
        userID = id;
    }
    public static Integer getNaposledZvolenyCvikId() { return naposledZvolenyCvikId; }
    public static void setNaposledZvolenyCvikId(Integer setId_cvik) { naposledZvolenyCvikId = setId_cvik; }
    public static Boolean isZvolenyCvikOblibeny() {
        return jeZvolenyCvikOblibeny;
    }
    public static void setJeZvolenyCvikOblibeny(Boolean booleanHodnota) {
        jeZvolenyCvikOblibeny = booleanHodnota;
    }
    public static void toggleJeZvolenyCvikOblibeny() {
        jeZvolenyCvikOblibeny = !jeZvolenyCvikOblibeny;
    }
}
