package cz.vse.fitness_buddy.coreFunc;
import java.sql.*;

/**
 * Funkce, ktera ma na starosti komunikaci s databazi.
 */
public class DatabazeRelay {
    private String ip;
    private String user;
    private String password;
    private String db;
    Connection connection;
    public DatabazeRelay(String ip, String user, String password, String db) throws SQLException {
        this.ip = ip;
        this.user = user;
        this.password = password;
        this.db = db;

        connection = DriverManager.getConnection("jdbc:mysql://" + this.ip + "/" + this.db, this.user, this.password);
    }

    /**
     * Sekce funkci, ktere primo komunikuji s databazi.
     * Konvence:
     *  - jmenoTabulky - jmeno tabulky, na kterou se dotazujeme/menime
     *  - jmenoSloupce - jmeno sloupce, o ktery se zajimame
     *  - hodnota - hodnota ve sloupci
     *    - pokud jsou argumenty funkce strukturovany jako (...,jmenoSloupce,hodnota,...) znamena to, ze hodnota se vztahuje ke jmenovanemu sloupci.
     *  - jmenoSloupceZiskany - jmeno sloupce, ze ktereho dostavame informaci
     *  - jmenoSloupceRidici - jmeno sloupce, podle jehoz hodnoty se v hledani ridime. Napr. chceme najit radek, kde ve sloupci x je hodnota y. Sloupec x je tedy ridici,
     *      pro rozliseni naproti ziskanemu.
     *  - jmenoSloupceKeZmene - jmeno sloupce, ktery v zaznamu menime.
     *  - hodnotaKUlozeni - hodnota, kterou se snazime ulozit.
     *    - opet plati, ze pokud je uvedeno (...,jmenoSloupceKeZmene,hodnotaKUlozeni,...), plati, ze hodnota se ulozi do jmenovaneho sloupce.
     */
    public int vlozUzivatele(String insertQuery) throws SQLException {
        int userId = -1;
        PreparedStatement pstmt = connection.prepareStatement(insertQuery, Statement.RETURN_GENERATED_KEYS);
        int affectedRows = pstmt.executeUpdate();
        if (affectedRows > 0) {
            ResultSet generatedKeys = pstmt.getGeneratedKeys();
            if (generatedKeys.next()) {
                userId = generatedKeys.getInt(1); // Retrieves the first auto-generated key
            }
        } else {
            System.out.println("No data inserted.");
        }

        pstmt.close();

        return userId;
    }
    public boolean existujeZaznam (String jmenoTabulky, String jmenoSloupce, String hodnota) throws SQLException {
        String query = "SELECT COUNT(*) FROM " + jmenoTabulky + " WHERE " + jmenoSloupce + " = ?";
        PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1,hodnota);

        ResultSet resultSet = statement.executeQuery();
        resultSet.next();
        return resultSet.getInt(1) > 0;
    }
    public String vratHodnotu (String jmenoTabulky, String jmenoSloupceRidici, String jmenoSloupceZiskany, String hodnota) throws SQLException {
        String query = "SELECT " + jmenoSloupceZiskany + " FROM " + jmenoTabulky + " WHERE " + jmenoSloupceRidici + " = ?";
        PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1,hodnota);

        ResultSet resultSet = statement.executeQuery();
        resultSet.next();
        return resultSet.getString(1);
    }
    public String vratHodnotuPodle2Sloupcu(String jmenoTabulky, String jmenoSloupceRidici1, String hodnota1, String jmenoSloupceRidici2, String hodnota2, String jmenoSloupceZiskany) throws SQLException {
        /*Kontrola prazdnych dat v tabulce oblibeny_cvik
        if (!(vratPocetRadku(jmenoTabulky) > 0)) { return ""; }*/
        String query = "SELECT " + jmenoSloupceZiskany + " FROM " + jmenoTabulky
                        + " WHERE " + jmenoSloupceRidici1 + " LIKE \"" + hodnota1 + "\" AND " + jmenoSloupceRidici2 + " LIKE \"" + hodnota2 + "\"";
        PreparedStatement statement = connection.prepareStatement(query);

        ResultSet resultSet = statement.executeQuery();
        resultSet.next();
        return resultSet.getString(1);
    }
    public ResultSet vratTabulku1Sloupec (String jmenoTabulky, String jmenoSloupce, boolean pouzitPodminku, String podminka) throws SQLException {
        String query = "SELECT " + jmenoSloupce + " FROM " + jmenoTabulky + (pouzitPodminku ? podminka : "");
        PreparedStatement statement = connection.prepareStatement(query);

        ResultSet resultSet = statement.executeQuery();
        return resultSet;
    }
    public ResultSet vratTabulku2Sloupce (String jmenoTabulky, String jmenoSloupce1, String jmenoSloupce2, boolean pouzitPodminku, String podminka) throws SQLException {
        String query = "SELECT " + jmenoSloupce1 + "," + jmenoSloupce2 + " FROM " + jmenoTabulky + (pouzitPodminku ? (" " + podminka) : "");
        PreparedStatement statement = connection.prepareStatement(query);

        ResultSet resultSet = statement.executeQuery();
        return resultSet;
    }
    public ResultSet vratRelaceCvikKategorie() throws SQLException {
        String query = "SELECT id_cvik, id_kategorie FROM cvik_kategorie";
        PreparedStatement statement = connection.prepareStatement(query);
        ResultSet resultSet = statement.executeQuery();
        return resultSet;
    }
    public int vratPocetRadku (String jmenoTabulky, boolean pouzitPodminku, String podminka) throws SQLException {
        String query = "SELECT COUNT(*) FROM " + jmenoTabulky + (pouzitPodminku ? " " + podminka : "");
        PreparedStatement statement = connection.prepareStatement(query);

        ResultSet resultSet = statement.executeQuery();
        resultSet.next();
        return resultSet.getInt(1);
    }
    public int vratPocetRadku (String jmenoTabulky) throws SQLException {
        return vratPocetRadku(jmenoTabulky,false,null);
    }
    public void vytvorOblibenyCvikZaznamy (int idUzivatele) throws SQLException {
        ResultSet idCviku = vratTabulku1Sloupec("cvik","id_cvik",false,null);
        int pocetCviku = vratPocetRadku("cvik");
        for (int i=0;i<pocetCviku;i++) {
            idCviku.next();
            PreparedStatement statement = connection.prepareStatement("INSERT INTO oblibeny_cvik (id_cviku,id_uzivatele,je_oblibeny,poznamka) VALUES (" + idCviku.getString(1) + "," + idUzivatele + ",0,\"\")");
            statement.execute();
        }
    }
    public void upravHodnotuPodle2Sloupcu(String jmenoTabulky, String jmenoSloupceRidici1, String jmenoSloupceRidici2, String jmenoSloupceKeZmene, String hodnotaKUlozeni) throws SQLException {
        String query = "UPDATE " + jmenoTabulky + " SET " + jmenoSloupceKeZmene + " = ? WHERE " + jmenoSloupceRidici1 + " = " + Core.getUserID() + " AND " + jmenoSloupceRidici2 + " = ?";
        PreparedStatement statement = connection.prepareStatement(query);
        if(jmenoSloupceKeZmene.equals("je_oblibeny")){
            statement.setString(1, hodnotaKUlozeni );
            statement.setString(2, String.valueOf(Core.getNaposledZvolenyCvikId()));
        } else {
            statement.setString(1, hodnotaKUlozeni);
            statement.setString(2, String.valueOf(Core.getNaposledZvolenyCvikId()));
        }
        statement.execute();
    }
    public void upravHodnotuPodle2Sloupcu(String jmenoTabulky, String jmenoSloupceRidici1, String hodnota1, String jmenoSloupceRidici2, String hodnota2, String jmenoSloupceKeZmene, String hodnotaKUlozeni) throws SQLException {
        String query = "UPDATE " + jmenoTabulky + " SET " + jmenoSloupceKeZmene + "=\"" + hodnotaKUlozeni + "\" WHERE " + jmenoSloupceRidici1 + "=\"" + hodnota1 + "\" AND " + jmenoSloupceRidici2 + "=\"" + hodnota2 + "\"";
        PreparedStatement statement = connection.prepareStatement(query);
        statement.execute();
    }
    public void upravHodnotu(String jmenoTabulky, String jmenoSloupceRidici, String hodnota, String jmenoSloupceKeZmene, String hodnotaKUlozeni) throws SQLException {
        String query = "UPDATE " + jmenoTabulky + " SET " + jmenoSloupceKeZmene + "=\"" + hodnotaKUlozeni + "\" WHERE " + jmenoSloupceRidici + " LIKE " + hodnota;
        PreparedStatement statement = connection.prepareStatement(query);
        statement.execute();
    }
}
