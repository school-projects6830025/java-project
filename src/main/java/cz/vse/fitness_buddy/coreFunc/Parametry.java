package cz.vse.fitness_buddy.coreFunc;

/**
 * globalni promenne.
 */
public class Parametry {
    public static final int vyskaOkna = 720;
    public static final int sirkaOkna = 1280;
    public static final int delkaFade = 3000; // millis, jak dlouho maji byt videt hlasky
}
