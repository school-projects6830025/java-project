package cz.vse.fitness_buddy.main;

import cz.vse.fitness_buddy.coreFunc.Core;
import javafx.scene.control.Button;
import javafx.scene.image.ImageView;

import java.sql.SQLException;

/**
 * Trida pouzivana v ListCells, ktere reprezentuji cvik, a to CvikListCell pouzity na obrazovce s prohlizenim cviku (ScreenNum.OB3), a oblibenyCvikListCell
 * pouzity na obrazovce s oblibenymi cviky (ScreenNum.OB4)
 */
public class CvikPolozka {
    private int id;
    private boolean oblibeny;

    public CvikPolozka(int id,boolean oblibeny) {
        this.id = id;
        this.oblibeny = oblibeny;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public boolean isOblibeny() {
        return oblibeny;
    }
}
