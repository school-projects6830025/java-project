package cz.vse.fitness_buddy.main;

import cz.vse.fitness_buddy.coreFunc.Core;
import cz.vse.fitness_buddy.coreFunc.Parametry;
import cz.vse.fitness_buddy.main.listCells.CvikListCell;
import javafx.animation.FadeTransition;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.scene.web.WebView;
import javafx.util.Duration;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;
import java.util.regex.Pattern;

/**
 *  Class CvikyController - controller, ktery ovlada hlavni obrazovku aplikace, OB3.
 *
 *  Tato třída inicializuje Scenu hlavni obrazovky s cviky.
 *  Třída má @FXML metody upravujici uzivatelske rozhrani.
 *  Metody používané pro zobrazeni seznamu cvik, jejich filtrovani budto podle textu nebo kategorii,
 *  zobrazovani detailu cvik pri jejich vyberu a ruzne funkce souvisejici s cviky.
 *  Pro prechod na obrazovku s oblibenymi cviky(OB4) nebo prihlasovani(OB1),
 *  a dodatecne zobrazovani ruznych hlasek pri uspesne nebo neuspesne akci.
 *
 *@author     Tomas Pydych, Jan Novak, Dao Sinh Duc
 *@version    pro školní rok 2022/2023
 */
public class CvikyController extends UberController {

    /**
     * FXML elementy
     */
    @FXML
    private Button ulozitPoznamkuButton;
    @FXML
    private ToggleGroup podminkaHledani;
    @FXML
    private TextField hledaciTextField;
    @FXML
    private Label vysledekHledani;
    @FXML
    private ListView cvikyListView;
    @FXML
    private Label cvikLabel;
    @FXML
    private Label hlaskaLabel;
    @FXML
    private TextArea poznamkaTextArea;
    @FXML
    private Button pridOdebOblibeneButton;
    @FXML
    private Button zobrazitOblibeneButton;
    @FXML
    private ToolBar tagyToolBar;
    @FXML
    private ImageView cvikImageView;
    @FXML
    private WebView cvikWebView;
    @FXML
    private RadioButton sjednoceniRadio;
    @FXML
    private RadioButton prunikRadio;
    @FXML
    private Menu vyberKategorieMenu;
    /**
     * Pomocne objekty
     */
    private Map<Integer,String> mapaKategorii = new HashMap<>();
    private Map<Integer,Boolean> jeKategorieZvolena = new HashMap<>();
    private Map<Integer,Set<Integer>> finalMapaRelaceCvikKategorie = new HashMap<>();
    private Map<Integer,String> finalMapaCviku = new HashMap<>();
    Set<Integer> zvoleneKategorie = new HashSet<>();
    private Set<Integer> setKategorieCvikyId = new HashSet<>();
    private Set<Integer> setVyhledatCvikyId = new HashSet<>();
    private Map<Integer,String> vyslednySeznamCviky = new HashMap<>();
    private ObservableList<CvikPolozka> cvikyCache = FXCollections.observableArrayList();
    /**
     * zacachovane UI obrazky, zamezime neustalemu cteni souboru
     * 0 - cancel cross, 1 - fav star off, 2 - fav star on
     */
    public static Image[] obrazky = {new Image(CvikyController.class.getResource("images/cancel_cross.png").toExternalForm()),new Image(CvikyController.class.getResource("images/fav_star_off.png").toExternalForm()),new Image(CvikyController.class.getResource("images/fav_star_on.png").toExternalForm())};

    /**
     * Prvotni inicializace datovych atributu tridy CvikyController.
     * @throws SQLException
     */
    @FXML
    private void initialize() throws SQLException {
        cvikWebView.getEngine().load(getClass().getResource("cviky/HTML/empty.html").toExternalForm());
        hlaskaLabel.setText("");

        ResultSet jmenaKategorii = Core.vratTabulku1Sloupec("kategorie","nazev_kategorie",false,null);
        ResultSet idKategorii = Core.vratTabulku1Sloupec("kategorie","id_kategorie",false,null);
        int len = Core.vratPocetRadku("kategorie");
        for (int i=0;i<len;i++) {
            jmenaKategorii.next();
            idKategorii.next();
            mapaKategorii.put(idKategorii.getInt(1),jmenaKategorii.getString(1));
            jeKategorieZvolena.put(idKategorii.getInt(1),false);
        }
        for (int i : jeKategorieZvolena.keySet()) vyberKategorieMenu.getItems().add(novaNevybranaKategorie(i));
        podminkaHledani.selectToggle(sjednoceniRadio);

        ResultSet pocetCvikuRelace = Core.vratRelaceCvikKategorie();
        pocetCvikuRelace.next();
        Integer tmpId = pocetCvikuRelace.getInt("id_cvik");
        finalMapaRelaceCvikKategorie.put(tmpId, new HashSet<>());
        do {
            int idCvik = pocetCvikuRelace.getInt("id_cvik");
            int idKategorie = pocetCvikuRelace.getInt("id_kategorie");
            if(tmpId.equals(idCvik)) {
                finalMapaRelaceCvikKategorie.get(idCvik).add(idKategorie);
            } else {
                finalMapaRelaceCvikKategorie.put(idCvik, new HashSet<>());
                finalMapaRelaceCvikKategorie.get(idCvik).add(idKategorie);
                tmpId = idCvik;
            }
        } while (pocetCvikuRelace.next());

        ResultSet nazvyCviku = Core.vratTabulku1Sloupec("cvik","nazev",false,null);
        ResultSet idCviku = Core.vratTabulku1Sloupec("cvik","id_cvik",false,null);
        len = Core.vratPocetRadku("cvik");
        for (int i=0;i<len;i++) {
            nazvyCviku.next();
            idCviku.next();
            finalMapaCviku.put(idCviku.getInt(1),nazvyCviku.getString(1));
        }
        setVyhledatCvikyId.addAll(finalMapaRelaceCvikKategorie.keySet());
        vyslednySeznamCviky.putAll(finalMapaCviku);

        cvikyListView.setItems(cvikyCache);
        cvikyListView.setCellFactory(param -> new CvikListCell());

        pridOdebOblibeneButton.setDisable(true);
        ulozitPoznamkuButton.setDisable(true);

        poznamkaTextArea.setText("");
    }
    /**
     * Aktualizuje a prizpusobi nam vzhled obrazovky s cviky(OB3) podle toho, jestli se uzivatel prihlasil nebo neprihlasil.
     * Zavola se na pocatku pred prechodem na obrazovku s Cviky(OB3).
     */
    @Override
    public void aktualizuj() {
        if (Core.isUzivatelPrihlasen()) {
            pridOdebOblibeneButton.setVisible(true);
            zobrazitOblibeneButton.setVisible(true);
            if (cvikyListView.getSelectionModel().getSelectedIndex() != -1) ulozitPoznamkuButton.setDisable(false);
            poznamkaTextArea.setPromptText("Zde si lze ke cviku napsat a uložit vlastní poznámku.");
        } else {
            pridOdebOblibeneButton.setVisible(false);
            zobrazitOblibeneButton.setVisible(false);
            ulozitPoznamkuButton.setDisable(true);
            poznamkaTextArea.setPromptText("Pro přidání poznámky se přihlaste.");
            poznamkaTextArea.setText("");
        }
        aktualizujKategorie();
    }
    /**
     * Metoda, ktera zaznamena kliknuti na vyber mezi "plati alespon jedna" a "plati vsechny" a provede zmenu v databaze i v aplikaci.
     * @param efekt, doplni informaci jak se nam hlaska ma zobrazit. Musi vychazet z vyctovyho typu MsgEffect.
     * @see MsgEffect
     * @param hlaska, parametr String urcuje text, ktery se ma zobrazit.
     */
    @Override
    public void zobrazHlasku(MsgEffect efekt, String hlaska) {
        FadeTransition fade = new FadeTransition(Duration.millis(Parametry.delkaFade));
        if (efekt == MsgEffect.SHORTFADE_GREEN || efekt == MsgEffect.SHORTFADE_RED) {
            fade.setFromValue(1.0);
            fade.setToValue(0.0);
            fade.setCycleCount(1);
            fade.setAutoReverse(false);
        }
        switch (efekt) {
            case SHORTFADE_RED -> {
                hlaskaLabel.setTextFill(Color.RED);
                fade.setNode(hlaskaLabel);
            }
            case SHORTFADE_GREEN -> {
                hlaskaLabel.setTextFill(Color.LIME);
                fade.setNode(hlaskaLabel);
            }
        }
        hlaskaLabel.setText(hlaska);
        fade.playFromStart();
    }
    /**
     * Zmeni nam informaci o nezvolenych druhu kategorii a vraci nam MenuItem, ktery se pote v aplikaci zobrazi
     * pod Menu "Vyberte kategorii.." jako MenuItem.
     * @param id, id_kategorie poskytujici informaci o jakou kategorii se jedna.
     * @return MenuItem, objekt, ktery nam pote vizualne zobrazi nezvolene kategorie jako MenuItem u Menu "Vyberte kategorii..".
     */
    private MenuItem novaNevybranaKategorie(int id) {
        MenuItem vyber = new MenuItem();
        vyber.setText(mapaKategorii.get(id));
        vyber.setUserData(id);
        vyber.setOnAction(e -> {
            jeKategorieZvolena.replace((Integer)vyber.getUserData(),true);
            aktualizujKategorie();
        });
        return vyber;
    }
    /**
     * Zmeni nam informaci o zvolenych druhu kategorii a vraci nam Button, ktery se pote v aplikaci zobrazi jako zvoleny.
     * @param id, id_kategorie poskytujici informaci o jakou kategorii se jedna.
     * @return Button objekt, ktery nam pote vizualne zobrazi zvolene kategorie.
     */
    private Button novaVybranaKategorie (int id) {
        Button vyber = new Button();
        vyber.setText(mapaKategorii.get(id));
        ImageView img = new ImageView();
        img.setFitHeight(10);
        img.setFitWidth(10);
        img.setImage(obrazky[0]);
        vyber.setGraphic(img);
        vyber.setUserData(id);
        vyber.setOnAction(e -> {
            jeKategorieZvolena.replace((Integer)vyber.getUserData(),false);
            aktualizujKategorie();
        });
        return vyber;
    }
    /**
     * Metoda, ktera zaznamena kliknuti na vyber mezi "plati alespon jedna" a "plati vsechny" a provede zmenu v databaze i v aplikaci.
     */
    @FXML
    private void getRadioButton(ActionEvent actionEvent) {
        aktualizujKategorie();
    }
    /**
     * Zavola se, aby se aplikovala logika ve zpusobu filtrovani cvik podle kategorii na "plati alespon jedna" a "plati vse".
     */
    private void aktualizujLogikuKategorie() {
        for (Integer kategorieId : jeKategorieZvolena.keySet()) {
            if (jeKategorieZvolena.get(kategorieId)) zvoleneKategorie.add(kategorieId);
        }
        if(prunikRadio.isSelected()) {
            for (Integer cvikaId : finalMapaRelaceCvikKategorie.keySet()) {
                if(finalMapaRelaceCvikKategorie.get(cvikaId).containsAll(zvoleneKategorie)) {
                    setKategorieCvikyId.add(cvikaId);
                }
            }
        } else if(sjednoceniRadio.isSelected()) {
            for (Integer cvikaId : finalMapaRelaceCvikKategorie.keySet()) {
                for (Integer kategorieId : zvoleneKategorie) {
                    if(finalMapaRelaceCvikKategorie.get(cvikaId).contains(kategorieId)) {
                        setKategorieCvikyId.add(cvikaId);
                        break;
                    }
                }
            }
        }
    }
    /**
     * Zavola se pri zmene ve zpusobu filtrovani cvik podle kategorii.
     * Metoda nam ulozi informace, ktere kategorie byly zvoleny a ktere ne.
     * Pote nam zavola metody, ktere nam ve vysledku vytvori filtrovany seznam cvik,
     */
    private void aktualizujKategorie() {
        zvoleneKategorie.clear();
        setKategorieCvikyId.clear();
        vyberKategorieMenu.getItems().clear();
        tagyToolBar.getItems().clear();
        for (Integer idKategorie : jeKategorieZvolena.keySet()) {
            if (!jeKategorieZvolena.get(idKategorie)) {
                vyberKategorieMenu.getItems().add(novaNevybranaKategorie(idKategorie));
            } else {
                tagyToolBar.getItems().add(novaVybranaKategorie(idKategorie));
            }
        }
        aktualizujLogikuKategorie();
        setVyslednySeznamCviky();
    }
    /**
     * Metoda, ktera zaznamena kliknuti na tlacitko "vyhledat" a provede filtrovani seznamu s cviky podle textu.
     * @param actionEvent
     */
    @FXML
    private void kliknutiNaVyhledat(ActionEvent actionEvent) {
        vyhledat();
        setVyslednySeznamCviky();
    }
    /**
     * Provadi se zde filtrovani cvik podle textu.
     */
    private void vyhledat() {
        String pattern;
        pattern = "^" + hledaciTextField.getText();
        Pattern p1 = Pattern.compile(pattern,Pattern.CASE_INSENSITIVE);
        pattern = "^.+ " + hledaciTextField.getText();
        Pattern p2 = Pattern.compile(pattern,Pattern.CASE_INSENSITIVE);
        setVyhledatCvikyId.clear();
        if (!hledaciTextField.getText().equals("")) {
            for (Integer id : finalMapaCviku.keySet()) {
                if (p1.matcher(finalMapaCviku.get(id)).lookingAt() || p2.matcher(finalMapaCviku.get(id)).lookingAt()) {
                    setVyhledatCvikyId.add(id);
                }
            }
        } else {
            vyhledatPrazdny();
        }
    }
    /**
     * Metoda, ktera zaznamena kliknuti na tlacitko s krizkem vedle pole vyhledavani.
     * Funkce tlacitka je, aby se jednoduse vymazalo "filtrovani" podle textu.
     * @param actionEvent
     */
    @FXML
    private void zrusitButton(ActionEvent actionEvent) {
        hledaciTextField.clear();
        vyhledatPrazdny();
    }

    private void vyhledatPrazdny() {
        vysledekHledani.setText("");
        setVyhledatCvikyId.addAll(finalMapaCviku.keySet());
        setVyslednySeznamCviky();
    }
    /**
     * Pouzito pri aplikaci filtrovani v seznamu cvik.
     * Metoda nam vytvori vysledny seznam cvik, ktere se ma zobrazit.
     */
    private void setVyslednySeznamCviky() {
        vyslednySeznamCviky.clear();
        if(setKategorieCvikyId.isEmpty()) {
            if(zvoleneKategorie.isEmpty()) {
                for (Integer idCvik : setVyhledatCvikyId) {
                    vyslednySeznamCviky.put(idCvik, finalMapaCviku.get(idCvik));
                }
            }
        } else {
            Set<Integer> intersection = new HashSet<Integer>(setVyhledatCvikyId);
            intersection.retainAll(setKategorieCvikyId);
            for (Integer idCvik : intersection) {
                vyslednySeznamCviky.put(idCvik, finalMapaCviku.get(idCvik));
            }
        }
        aktualizujSeznamCviku();
    }
    /**
     * Aktualizuje seznam cviku pri aplikaci jednoho z filtru, a nebo pri pridani/odebrani cvika do/z oblibene.
     */
    private void aktualizujSeznamCviku () {
        cvikyCache.clear();

        if (Core.isUzivatelPrihlasen()) {
            Map<Integer,Boolean> oblibene = new HashMap<>();
            ResultSet set;
            int len;
            try {
                set = Core.vratTabulku2Sloupce("oblibeny_cvik","id_cviku","je_oblibeny",true,"WHERE id_uzivatele LIKE " + Core.getUserID());
                len = Core.vratPocetRadku("oblibeny_cvik",true,"WHERE id_uzivatele LIKE " + Core.getUserID());
                for (int i=0;i<len;i++) {
                    set.next();
                    oblibene.put(set.getInt(1),set.getString(2).equals("1"));
                }
            } catch (SQLException e) {
                throw new RuntimeException(e);
            }
            for (Integer id : vyslednySeznamCviky.keySet()) {
                cvikyCache.add(new CvikPolozka(id,oblibene.get(id)));
            }
        } else {
            for (Integer id : vyslednySeznamCviky.keySet()) {
                cvikyCache.add(new CvikPolozka(id,false));
            }
        }

        vysledekHledani.setText(cvikyCache.size() == 0 ? "Hledání neodpovídají žádné položky." : "");
    }
    /**
     * Urcuje, co se stane, pokud klikneme kamkoliv do cvikyListView - seznamu cviku.
     * @param mouseEvent
     * @throws SQLException
     */
    @FXML
    public void kliknutiNaCvik(MouseEvent mouseEvent) throws SQLException {
        if (cvikyListView.getSelectionModel().getSelectedIndex() == -1) return;
        ulozitPoznamkuButton.setDisable(false);
        pridOdebOblibeneButton.setDisable(false);

        Core.setNaposledZvolenyCvikId(cvikyCache.get(cvikyListView.getSelectionModel().getSelectedIndex()).getId());
        if (Core.isUzivatelPrihlasen()) {
            Core.setJeZvolenyCvikOblibeny(Core.vratHodnotuPodle2Sloupcu("oblibeny_cvik", "id_uzivatele", String.valueOf(Core.getUserID()), "id_cviku", String.valueOf(Core.getNaposledZvolenyCvikId()), "je_oblibeny").equals("1"));
        }
        aktualizujZobrazenyCvik();
    }

    /**
     * Aktualizuje obrazek, nadpis, webview, tlacitko pridani do oblibenych, a poznamku, podle toho, jake id cviku je ulozeno v Core.naposledZvolenyCvikId.
     * @throws SQLException
     */
    private void aktualizujZobrazenyCvik() throws SQLException {
        cvikLabel.setText(finalMapaCviku.get(Core.getNaposledZvolenyCvikId()));
        Image image;
        try {
            image = new Image(getClass().getResourceAsStream("cviky/images/" + Core.getNaposledZvolenyCvikId() + ".png"),512, 512, false, false);
        } catch (NullPointerException e) {
            image = new Image(getClass().getResourceAsStream("cviky/images/image_missing.png"),512, 512, false, false);
        }
        cvikImageView.setImage(image);
        cvikWebView.getEngine().load(getClass().getResource("cviky/HTML/" + Core.getNaposledZvolenyCvikId() + ".html").toExternalForm());
        if(Core.isUzivatelPrihlasen()) {
            zobrazPoznamku();
            aktualizujTlacitka();
        }
    }
    /** co se stane pri kliknuti na button */
    @FXML
    private void pridOdebOblibene(ActionEvent actionEvent) throws SQLException {
        if(Core.isZvolenyCvikOblibeny()) {
            Core.upravHodnotuPodle2Sloupcu("oblibeny_cvik", "id_uzivatele", "id_cviku", "je_oblibeny", "0");
        } else {
            Core.upravHodnotuPodle2Sloupcu("oblibeny_cvik", "id_uzivatele", "id_cviku", "je_oblibeny", "1");
        }
        Core.toggleJeZvolenyCvikOblibeny();
        aktualizujTlacitka();
        aktualizujSeznamCviku();
    }
    /**
     * Metoda pro aktualizovani tlacitka pridat/odebrat oblibene.
     */
    private void aktualizujTlacitka( ) {
        if (!Core.isUzivatelPrihlasen()) return;
        if (Core.isZvolenyCvikOblibeny()) {
            pridOdebOblibeneButton.setText("Odebrat z oblíbených");
        } else {
            pridOdebOblibeneButton.setText("Přidat do oblíbených");
        }
    }
    /**
     * Metoda, ktera zaznamena kliknuti na tlacitko "ulozit poznamku" a provede zmenu v  databaze i v aplikaci.
     * @param actionEvent
     * @throws SQLException
     */
    @FXML
    private void ulozPoznamku(ActionEvent actionEvent) throws SQLException {
        String hodnotaKUlozeni = poznamkaTextArea.getText();
        int delka = hodnotaKUlozeni.length();
        if(delka > 255) {
            zobrazHlasku(MsgEffect.SHORTFADE_RED,"Text má délku " + delka + " a maximální přesahuje délku 255znaků, text zkraťte.");
        } else {
            Core.upravHodnotuPodle2Sloupcu("oblibeny_cvik", "id_uzivatele", "id_cviku", "poznamka", hodnotaKUlozeni);
            zobrazPoznamku();
            zobrazHlasku(MsgEffect.SHORTFADE_GREEN,"Poznámka uložena.");
        }
    }
    /**
     * Metoda, ktera v aplikaci zobrazi poznamku vybraneho cviku ulozene v databazi.
     * @throws SQLException
     */
    private void zobrazPoznamku( ) throws SQLException {
        String poznamka = Core.vratHodnotuPodle2Sloupcu("oblibeny_cvik", "id_uzivatele", String.valueOf(Core.getUserID()), "id_cviku", String.valueOf(Core.getNaposledZvolenyCvikId()),"poznamka");
        poznamkaTextArea.setText(poznamka);
    }
    /**
     * Metoda, ktera vraci obrazek k zobrazeni pri kliknuti na cvik.
     * @see CvikListCell
     * @param  index, pozice elementu
     * @return Image, obrazek k zobrazeni
     */
    public static Image vratObrazek(int index) {
        return (index < obrazky.length && index >= 0) ? obrazky[index] : null;
    }
    /**
     * Metoda, ktera zaznamena kliknuti na tlacitko "zobrazit oblibene" a zobrazi veskere oblibene cviky uzivatele.
     * @param actionEvent
     */
    public void prejitNaOblibene(ActionEvent actionEvent) {
        prejitNaObrazovku(ScreenNum.OB4);
    }
}
