package cz.vse.fitness_buddy.main;

import cz.vse.fitness_buddy.coreFunc.Core;
import cz.vse.fitness_buddy.coreFunc.Parametry;
import javafx.animation.FadeTransition;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.image.Image;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import javafx.util.Duration;

import java.sql.SQLException;
import java.util.Optional;

/**
 * Trida, ktera definuje fungovani ovladacich prvku na obrazovce "detail uctu" (ScreenNum.OB5)
 */
public class DetUctuController extends UberController {
    @FXML
    private Label hlaskaLabel;
    @FXML
    private PasswordField zmenaHeslaField;
    @FXML
    private Label uzivJmenoLabel;
    @FXML
    private void initialize () {
        hlaskaLabel.setText("");
    }
    @FXML
    private void pokusZmenitHeslo(ActionEvent actionEvent) {
        String noveHeslo = zmenaHeslaField.getText();
        if (noveHeslo.equals("")) {
            zobrazHlasku(MsgEffect.SHORTFADE_RED,"je třeba vyplnit pole.");
            return;
        }
        zmenaHeslaField.clear();
        try {
            Core.upravHodnotu("registrovany_uzivatel","id_uziv",String.valueOf(Core.getUserID()),"heslo",noveHeslo);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        zobrazHlasku(MsgEffect.SHORTFADE_GREEN,"heslo úspěšně změněno.");
    }
    @FXML
    private void odhlasitSe(ActionEvent actionEvent) {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION,"Proveďte kliknutím na OK.");
        alert.setTitle("Odhlásit se");
        ((Stage)alert.getDialogPane().getScene().getWindow()).getIcons().add(new Image(FitBudMain.class.getResourceAsStream("images/lock.png")));
        alert.setHeaderText("Opravdu se chcete odhlásit?");
        Optional<ButtonType> result = alert.showAndWait();
        if (result.isPresent() && result.get() == ButtonType.OK) {
            Core.setUzivatelPrihlasen(false);
            prejitNaObrazovku(ScreenNum.OB1);
        }
    }
    @FXML
    private void prejitNaSeznamCviku(ActionEvent actionEvent) {
        prejitNaObrazovku(ScreenNum.OB3);
    }
    @Override
    public void aktualizuj() {
        String getJmeno;
        try {
            getJmeno = Core.vratHodnotu("registrovany_uzivatel","id_uziv","uzivatelske_jmeno",String.valueOf(Core.getUserID()));
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        uzivJmenoLabel.setText(getJmeno);
    }
    @Override
    public void zobrazHlasku(MsgEffect efekt, String hlaska) {
        FadeTransition fade1 = new FadeTransition(Duration.millis(Parametry.delkaFade));
        if (efekt == MsgEffect.SHORTFADE_GREEN || efekt == MsgEffect.SHORTFADE_RED) {
            fade1.setFromValue(1.0);
            fade1.setToValue(0.0);
            fade1.setCycleCount(1);
            fade1.setAutoReverse(false);
        }
        switch (efekt) {
            case SHORTFADE_RED -> {
                hlaskaLabel.setTextFill(Color.RED);
                fade1.setNode(hlaskaLabel);
            }
            case SHORTFADE_GREEN -> {
                hlaskaLabel.setTextFill(Color.LIME);
                fade1.setNode(hlaskaLabel);
            }
        }
        if (efekt == MsgEffect.SHORTFADE_GREEN || efekt == MsgEffect.SHORTFADE_RED) fade1.playFromStart();
        hlaskaLabel.setText(hlaska);
    }
}
