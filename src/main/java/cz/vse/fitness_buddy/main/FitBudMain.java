package cz.vse.fitness_buddy.main;

import cz.vse.fitness_buddy.coreFunc.Parametry;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.text.Font;
import javafx.stage.Stage;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class FitBudMain extends Application {
    private static Stage mainStage;
    private Map<ScreenNum,Scene> sceny = new HashMap<>();
    private Map<ScreenNum,UberController> controllery = new HashMap<>();

    /**
     * Startovaci funkce. Pouzivame setUserData(this) abychom uchovali informaci o instanci okna, na ktere se pak dotazujeme, pokud chceme prepinat mezi obrazovkami
     * Ve startovaco funkci nastavujeme i ikonu a nadpis okna. Zaroven volame funkci nactiSceny();
     * @param stage
     * @throws IOException
     */
    @Override
    public void start(Stage stage) throws IOException {
        mainStage = stage;

        Font.loadFont(getClass().getResourceAsStream("/cz/vse/fitness_buddy/main/fonts/Roboto-Regular.ttf"), 20);
        Font.loadFont(getClass().getResourceAsStream("/cz/vse/fitness_buddy/main/fonts/Roboto-Regular.ttf"), 20);

        nactiSceny();

        mainStage.setUserData(this);
        mainStage.setTitle("Fitness Buddy");
        mainStage.getIcons().add(new Image(FitBudMain.class.getResourceAsStream("images/dumbell_64.png")));
        mainStage.setScene(sceny.get(ScreenNum.OB1));
        mainStage.setOnCloseRequest(e -> {
            Platform.exit();
        });
        mainStage.show();
    }
    /**
     * Funkce, ktera nacte vsechny obrazovky. Vsechny instance scen da do mapy, stejne tak controllery. Controllery maji rodicovskou abstraktni tridu UberController,
     * aby toto bylo mozne udelat, a aby bylo mozne na controllerech volat aktualizuj().
     * */
    private void nactiSceny () throws IOException {
        FXMLLoader loader;
        for (ScreenNum var : ScreenNum.values()) {
            loader = new FXMLLoader(FitBudMain.class.getResource("layouts/" + var.jmeno + ".fxml"));
            sceny.put(var,new Scene(loader.load(), Parametry.sirkaOkna, Parametry.vyskaOkna));
            controllery.put(var,loader.getController());
        }
    }

    public static Stage getStage() {
        return mainStage;
    }

    /**
     * Změní obrazovku na tu, přiřazenou k prvku enumerace ScreenNum, kterou funkce dostane jako proměnnou var
     * a zavolá aktualizuj funkci u controlleru
     *
     * @param var číslo obrazovky jako člen enumerace
     */
    public void prejitNaObrazovku (ScreenNum var) {
        if (controllery.get(var)!=null) controllery.get(var).aktualizuj();
        mainStage.setScene(sceny.get(var));
        controllery.get(ScreenNum.OB7).aktualizuj();
    }
    public static void main(String[] args) {
        launch();
    }
}