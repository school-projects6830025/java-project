package cz.vse.fitness_buddy.main;

import cz.vse.fitness_buddy.coreFunc.Core;
import cz.vse.fitness_buddy.coreFunc.Parametry;
import javafx.animation.FadeTransition;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.paint.Color;
import javafx.util.Duration;

import java.sql.SQLException;
/**
 *  Class HomeController - controller, ktery ovlada domovskou obrazovku, OB1.
 *
 *  Tato třída inicializuje Scenu obrazovky pro prihlasovani.
 *  Třída má metody používané pro prechod na obrazovku s cviky(OB3)- po prihlasovani s uctem i bez uctu,
 *  pro prechod na obrazovku OB2 pro registrovani a zobrazovani hlasek pri uspesnem nebo neuspesnem prihlasovani.
 *
 *@author     Tomas Pydych, Jan Novak, Dao Sinh Duc
 *@version    pro školní rok 2022/2023
 */
public class HomeController extends UberController {
    @FXML
    private TextField uzivJmenoField;
    @FXML
    private PasswordField hesloField;
    @FXML
    private Label hlaskaLabel;

    @FXML
    private void initialize() {
        Core.setUzivatelPrihlasen(false);
        hlaskaLabel.setText("");
    }
    /**
     * Metoda, ktera zaznamena kliknuti na tlacitko "Prihlasit".
     * @param actionEvent
     */
    @FXML
    private void prejdiNaCviky(ActionEvent actionEvent) {
        prejitNaObrazovku(ScreenNum.OB3);
    }

    /**
     * Metoda, ktera zaznamena kliknuti na tlacitko "Registrovat se".
     * @param actionEvent
     */
    @FXML
    private void prejitNaRegistraci(ActionEvent actionEvent) {
        prejitNaObrazovku(ScreenNum.OB2);
    }

    @Override
    public void aktualizuj() {

    }
    @Override
    public void zobrazHlasku(MsgEffect efekt, String hlaska) {
        FadeTransition fade1 = new FadeTransition(Duration.millis(Parametry.delkaFade));
        if (efekt == MsgEffect.SHORTFADE_GREEN || efekt == MsgEffect.SHORTFADE_RED) {
            fade1.setFromValue(1.0);
            fade1.setToValue(0.0);
            fade1.setCycleCount(1);
            fade1.setAutoReverse(false);
        }
        switch (efekt) {
            case SHORTFADE_RED -> {
                hlaskaLabel.setTextFill(Color.RED);
                fade1.setNode(hlaskaLabel);
            }
            case SHORTFADE_GREEN -> {
                hlaskaLabel.setTextFill(Color.LIME);
                fade1.setNode(hlaskaLabel);
            }
        }
        if (efekt == MsgEffect.SHORTFADE_GREEN || efekt == MsgEffect.SHORTFADE_RED) fade1.playFromStart();
        hlaskaLabel.setText(hlaska);
    }

    private void vyprazdniVsechnaPole() {
        uzivJmenoField.setText("");
        hesloField.setText("");
    }
    /**
     * Metoda, ktera zaznamena kliknuti na tlacitko "Prihlasit".
     * Pri uspesnem prihlaseni existujicim uctem nam metoda nastavi informace o prihlasenem uzivateli a provede prechod na hlavni obrazovku aplikace(OB3).
     * Pri neuspesnem prihlaseni nam zavola metodu zobrazHlasku a vypise ruzne hlasky podle situace.
     * @param actionEvent
     * @throws SQLException
     */
    @FXML
    private void prihlasitSe(ActionEvent actionEvent) throws SQLException {
        String jmeno = uzivJmenoField.getText();
        String heslo = hesloField.getText();
        if (jmeno.equals("") || heslo.equals("")) {
            zobrazHlasku(MsgEffect.SHORTFADE_RED,"Je třeba vyplnit všechna pole..");
        } else if (Core.existujeZaznam("registrovany_uzivatel", "uzivatelske_jmeno", jmeno)){
            if (heslo.equals(Core.vratHodnotu("registrovany_uzivatel", "uzivatelske_jmeno", "heslo", jmeno))) {
                Core.setUserID(Integer.parseInt(Core.vratHodnotu("registrovany_uzivatel", "uzivatelske_jmeno", "id_uziv", jmeno)));
                Core.setUzivatelPrihlasen(true);
                prejitNaObrazovku(ScreenNum.OB3);
                vyprazdniVsechnaPole();
            } else {
                zobrazHlasku(MsgEffect.SHORTFADE_RED,"Nesprávné heslo.");
            }
        } else {
            zobrazHlasku(MsgEffect.SHORTFADE_RED,"Účet s takovým jménem neexistuje.");
        }

    }
}

