package cz.vse.fitness_buddy.main;

import cz.vse.fitness_buddy.coreFunc.Core;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Menu;
import javafx.scene.image.Image;
import javafx.scene.web.WebView;
import javafx.stage.Stage;

import java.util.Optional;

/**
 *  Class MenuController - controller, ktery ovlada menu, ktere se zobrazuje na kazde obrazovce.
 *
 *  Tato třída inicializuje Menu Buttons aplikace.
 *  Třída má metody používané odhlaseni, ukonceni aplikace, zobrazeni oblibenych cvik.
 *
 *@author     Tomas Pydych, Jan Novak, Dao Sinh Duc
 *@version    pro školní rok 2022/2023
 */
public class MenuController extends UberController {

    @FXML
    private Menu oblibeneMenu;
    @FXML
    private Menu ucetMenu;
    @FXML
    private void prejitNaDetailUctu(ActionEvent actionEvent) {
        prejitNaObrazovku(ScreenNum.OB5);
    }
    @FXML
    private void initialize () { aktualizuj(); }

    /**
     * stara se o to, aby byly na liste zobrazene spravne moznosti, podle toho, zda je nekdo prihlaseny, ci ne.
     */
    @Override
    public void aktualizuj () {
        if (Core.isUzivatelPrihlasen()) {
            oblibeneMenu.setVisible(true);
            ucetMenu.setVisible(true);
        } else {
            oblibeneMenu.setVisible(false);
            ucetMenu.setVisible(false);
        }
        Platform.runLater(() -> aktualizuj());
    }

    @Override
    public void zobrazHlasku(MsgEffect efekt, String hlaska) {}
    public void prejitNaOblibene(ActionEvent actionEvent) {
        prejitNaObrazovku(ScreenNum.OB4);
    }
    public void prejitNaPrihlaseni(ActionEvent actionEvent) {
        prejitNaObrazovku(ScreenNum.OB1);

    }
    public void odhlaseni(ActionEvent actionEvent) {
        Core.setUzivatelPrihlasen(false);
        prejitNaPrihlaseni(actionEvent);
    }

    /**
     * vyhodi popup okno, ktere se pta, zda chceme aplikaci opravdu ukoncit. Pokud potrvrdime, zavre aplikaci
     *
     * @param actionEvent
     */
    @FXML
    private void ukoncit(ActionEvent actionEvent) {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION,"Proveďte kliknutím na OK.");
        alert.setTitle("Ukončit");
        ((Stage)alert.getDialogPane().getScene().getWindow()).getIcons().add(new Image(FitBudMain.class.getResourceAsStream("images/exiticon.png")));
        alert.setHeaderText("Opravdu ukončit?" + (Core.isUzivatelPrihlasen() ? " Budete automaticky odhlášeni." : "" ));
        Optional<ButtonType> result = alert.showAndWait();
        if (result.isPresent() && result.get() == ButtonType.OK) {
            Platform.exit();
        }
    }

    public void zobrazNapovedu(ActionEvent actionEvent) {
        Stage napovedaOkno = new Stage();
        WebView napovedaHTML = new WebView();
        napovedaOkno.setScene(new Scene(napovedaHTML));
        napovedaOkno.setTitle("Nápověda");
        napovedaOkno.show();
        napovedaOkno.getIcons().add(new Image(FitBudMain.class.getResourceAsStream("images/info.png")));
        napovedaHTML.getEngine().load(getClass().getResource("misc/Napoveda.html").toExternalForm());
    }
}
