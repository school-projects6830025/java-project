package cz.vse.fitness_buddy.main;
/**
 *  Class MsgEffect - vyctovy typ pro zpusob zobrazovani hlasek v aplikaci.
 *
 *@author     Tomas Pydych, Jan Novak, Dao Sinh Duc
 *@version    pro školní rok 2022/2023
 */
public enum MsgEffect {
    SHORTFADE_RED,
    SHORTFADE_GREEN
}
