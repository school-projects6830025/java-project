package cz.vse.fitness_buddy.main;

import cz.vse.fitness_buddy.coreFunc.Core;
import cz.vse.fitness_buddy.main.listCells.oblibenyCvikListCell;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.ListView;
import javafx.scene.input.MouseEvent;

import java.sql.ResultSet;
import java.sql.SQLException;

public class OblibeneController extends UberController {
    /**
     * ListView, ve kterem se zobrazi oblibene cviky.
     */
    @FXML
    private ListView oblibeneListView;
    /**
     * List, ktery obsahuje cviky, ktere chceme zobrazit. Konkretne vsechny cviky, ktere ma prihlaseny uzivatel oznacene jako oblibene.
     * Obsahem tohoto listu se ridi oblibeneListView.
     */
    private ObservableList<CvikPolozka> zobrazene = FXCollections.observableArrayList();
    /**
     * Inicializacni funkce, urci, ze prvky v oblibeneListView se maji inicializovat jako ListCell definovany v oblibenyCvikListCell, a obsah ListView se ma ridit
     * Listem zobrazene.
     */
    @FXML
    private void initialize () {
        oblibeneListView.setCellFactory(param -> new oblibenyCvikListCell());
        oblibeneListView.setItems(zobrazene);
        aktualizuj();
    }
    /**
     * obnovi obsah Listu zobrazene podle informaci z databaze.
     */
    @Override
    public void aktualizuj() {
        zobrazene.clear();
        if (Core.isUzivatelPrihlasen()) {
            ResultSet oblCviky;
            int len;
            try {
                oblCviky = Core.vratTabulku2Sloupce("oblibeny_cvik", "id_cviku", "je_oblibeny",true, "WHERE id_uzivatele=" + Core.getUserID());
                len = Core.vratPocetRadku("oblibeny_cvik",true,"WHERE id_uzivatele LIKE " + Core.getUserID());
                for (int i=0;i<len;i++) {
                    oblCviky.next();
                    if (oblCviky.getString(2).equals("1")) zobrazene.add(new CvikPolozka(oblCviky.getInt(1),true));
                }
            } catch (SQLException e) {
                throw new RuntimeException(e);
            }
        }
    }
    @Override
    public void zobrazHlasku(MsgEffect efekt, String hlaska) {}
    public void prejitNaCviky(ActionEvent actionEvent) {
        prejitNaObrazovku(ScreenNum.OB3);
    }

    /**
     * urcuje, co se stane, pokud uzivatel klikne kamkoliv do oblibeneListView.
     * @param mouseEvent
     */
    public void klikNaSeznam(MouseEvent mouseEvent) {
        aktualizuj();
    }
}
