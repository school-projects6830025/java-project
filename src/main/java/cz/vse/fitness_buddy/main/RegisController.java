package cz.vse.fitness_buddy.main;

import cz.vse.fitness_buddy.coreFunc.Core;
import cz.vse.fitness_buddy.coreFunc.Parametry;
import javafx.animation.FadeTransition;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.paint.Color;
import javafx.util.Duration;

import java.sql.SQLException;

/**
 *  Class RegisController - controller, ktery ovlada obrazovku pro registraci, OB2.
 *
 *  Tato třída inicializuje Scenu obrazovky pro registraci.
 *  Třída má metody používané pro registraci uzivatele. Dale pro prechod na obrazovku s cviky(OB3)- po uspesne registraci uctu
 *  nebo zpet na obrazovku prihlasovani(OB1) - za pomoci MenuTlacitek.
 *
 *@author     Tomas Pydych, Jan Novak, Dao Sinh Duc
 *@version    pro školní rok 2022/2023
 */
public class RegisController extends UberController {
    @FXML
    private Label chybaRegistraceLabel;
    @FXML
    private TextField emailField;
    @FXML
    private PasswordField hesloField;
    @FXML
    private TextField uzivJmenoField;
    @FXML
    private void prejdiNaPrihlaseni(ActionEvent actionEvent) {
        prejitNaObrazovku(ScreenNum.OB1);
    }
    /**
     * Metoda, ktera zaznamena kliknuti na tlacitko "Registrovat se".
     * Pri uspesne registraci unikatniho uctu nam metoda provede ulozeni uzivatelskych udaju do databaze a provede prihlaseni
     * uzivatele - nastavi informace o prihlasenem uzivateli a provede prechod na hlavni obrazovku aplikace(OB3).
     * Pri neuspesne registraci nam zavola metodu zobrazHlasku a vypise ruzne hlasky podle situace.
     * @param actionEvent
     * @throws SQLException
     */
    @FXML
    private void registrovat(ActionEvent actionEvent) throws SQLException {
        String jmeno = uzivJmenoField.getText();
        String email = emailField.getText();
        String heslo = hesloField.getText();
        if (jmeno.equals("") || email.equals("") || heslo.equals("")) {
            zobrazHlasku(MsgEffect.SHORTFADE_RED, "Je třeba vyplnit všechna pole.");
        } else if (Core.existujeZaznam("registrovany_uzivatel", "uzivatelske_jmeno", jmeno)) {
            zobrazHlasku(MsgEffect.SHORTFADE_RED, "Takové jméno už existuje..");
        } else if (Core.existujeZaznam("registrovany_uzivatel", "email", email)) {
            zobrazHlasku(MsgEffect.SHORTFADE_RED, "Takový e-mail už existuje..");
        } else if (Core.existujeZaznam("registrovany_uzivatel", "heslo", heslo)) {
            zobrazHlasku(MsgEffect.SHORTFADE_RED, "Takové heslo už existuje..");
        } else {
            vyprazdniVsechnaPole();
            int id = Core.vlozUzivatele(jmeno, email, heslo);
            zobrazHlasku(MsgEffect.SHORTFADE_GREEN,"Registrace proběhla úspěšně.");
            Core.vytvorOblibenyCvikZaznamy(id);
            Core.setUserID(id);
            Core.setUzivatelPrihlasen(true);
            prejitNaObrazovku(ScreenNum.OB3);
        }
    }

    @Override
    public void aktualizuj () {
        chybaRegistraceLabel.setText("");
        vyprazdniVsechnaPole();
    }

    @Override
    public void zobrazHlasku(MsgEffect efekt, String hlaska) {
        FadeTransition fade1 = new FadeTransition(Duration.millis(Parametry.delkaFade));
        if (efekt == MsgEffect.SHORTFADE_GREEN || efekt == MsgEffect.SHORTFADE_RED) {
            fade1.setFromValue(1.0);
            fade1.setToValue(0.0);
            fade1.setCycleCount(1);
            fade1.setAutoReverse(false);
        }
        switch (efekt) {
            case SHORTFADE_RED -> {
                chybaRegistraceLabel.setTextFill(Color.RED);
                fade1.setNode(chybaRegistraceLabel);
            }
            case SHORTFADE_GREEN -> {
                chybaRegistraceLabel.setTextFill(Color.LIME);
                fade1.setNode(chybaRegistraceLabel);
            }
        }
        chybaRegistraceLabel.setText(hlaska);
        fade1.playFromStart();
    }

    private void vyprazdniVsechnaPole () {
        uzivJmenoField.clear();
        emailField.clear();
        hesloField.clear();
    }
}
