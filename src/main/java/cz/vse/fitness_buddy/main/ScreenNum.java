package cz.vse.fitness_buddy.main;

/**
 *  Enumerace, která je seznamem obrazovek v aplikaci. K číslům obrazovek jsou přiřazena jména .fxml souborů (ve stringu),
 *  korespondujících obrazovek.
 */
public enum ScreenNum {
    OB1("home"),
    OB2("registrace"),
    OB3("seznamCviku"),
    OB4("oblibeneCviky"),
    OB5("detailUctu"),
    OB7("menu"); // nepouzivame jako separatni obrazovku, misto toho je v horni liste ostatnich obrazovek
    public final String jmeno;
    ScreenNum(String jmeno) {
        this.jmeno = jmeno;
    }
}
