package cz.vse.fitness_buddy.main;

import javafx.fxml.FXML;
import javafx.stage.Stage;

/**
 * Abstraktni trida, z niz dedi vsechny controllery. Existuje hlavne kvuli prakticnosti pri prepinani mezi obrazovkami, a kvuli
 * jednotnosti nektere funkcionality vsech controlleru.
 */
public abstract class UberController {
    /**
     * proměnná zajišťuje, že každý controller bude mít odkaz na hlavní okno. Tato proměnná má v atributu userData odkaz na instanci
     * FitBudMain, ze které je prvotně spouštěna
     */
    @FXML
    private static final Stage primaryStage = FitBudMain.getStage();
    /**
     * u každé obrazovky bude nutná funkce aktualizuj, která upraví prvky podle příslušných proměnných, např. zda jsou vidět tlačítka
     * spojená s přihlášeným/nepřihlášeným uživatelem
     *
     */
    public abstract void aktualizuj();

    /**
     * u obrazovek kde je nutné, bude tato funkce zobrazovat stavové hlášky.
     *
     * @param efekt přídatná informace k zobrazení, např. pro předání barvy, kterou má hláška mít
     * efekt:
     *  SHORTFADE_RED: red text fadeout
     *  SHORTFADE_GREEN: green text fadeout
     *
     * @param hlaska String, který se vypíše jako hláška
     */
    public abstract void zobrazHlasku(MsgEffect efekt,String hlaska);
    /**
     * pomoci teto funkce ziskavaji controllery odkaz na hlavni okno aplikace.
     * @return instance okna
     */
    public static Stage getPrimaryStage() {
        return primaryStage;
    }
    /**
     * zavolá funkci hlavní třídy, která přejde na specifikovanou obrazovku v hlavním okně
     *
     * @param snum označení specifikované obrazovky v enumeraci
     */
    public void prejitNaObrazovku(ScreenNum snum) {
        FitBudMain main = (FitBudMain) getPrimaryStage().getUserData();
        main.prejitNaObrazovku(snum);
    }
}
