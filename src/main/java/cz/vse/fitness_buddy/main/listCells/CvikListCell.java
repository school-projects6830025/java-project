package cz.vse.fitness_buddy.main.listCells;

import cz.vse.fitness_buddy.coreFunc.Core;
import cz.vse.fitness_buddy.main.CvikPolozka;
import cz.vse.fitness_buddy.main.CvikyController;
import javafx.scene.control.ListCell;
import javafx.scene.image.ImageView;

import java.sql.SQLException;

/**
 * definuje prvky cvikyListView na obrazovce s prohlizenim cviku (ScreenNum.OB3)
 * pokud neni nikdo prihlasen, prvky jsou pouze jmena cviku. Pokud je uzivatel prihlaseny, zobrazi se u kazdeho prvku
 * hvezdicka, ktera ma zhasnuty vzhled pokud cvik neni v oblibenych, a rozsviceny vzhled v opacnem pripade.
 */
public class CvikListCell extends ListCell<CvikPolozka> {
    @Override
    protected void updateItem(CvikPolozka cvik, boolean empty) {
        super.updateItem(cvik, empty);
        if (empty) {
            setText(null);
            setGraphic(null);
        } else {
            try {
                setText(Core.vratHodnotu("cvik", "id_cvik", "nazev", String.valueOf(cvik.getId())));
            } catch (SQLException e) {
                throw new RuntimeException(e);
            }
            if (Core.isUzivatelPrihlasen()) {
                ImageView var = new ImageView(CvikyController.vratObrazek(cvik.isOblibeny() ? 2 : 1));
                var.setFitHeight(15);
                var.setFitWidth(15);
                setGraphic(var);
            }
        }
    }
}
