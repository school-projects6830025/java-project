package cz.vse.fitness_buddy.main.listCells;

import cz.vse.fitness_buddy.coreFunc.Core;
import cz.vse.fitness_buddy.main.CvikPolozka;
import cz.vse.fitness_buddy.main.CvikyController;
import javafx.scene.control.Button;
import javafx.scene.control.ListCell;
import javafx.scene.image.ImageView;

import java.sql.SQLException;
/**
 * ListCell, ktery se zobrazuje jako prvek v seznamu oblibenych cviku (oblibeneListView) na obrazovce oblibene cviky (ScreenNum.OB4).
 * Zobrazuje se jako radek tabulky, kazdy radek obsahuje jmeno cviku a tlacitko, kterym tento cvik odstranime z oblibenych.
 * */
public class oblibenyCvikListCell extends ListCell<CvikPolozka> {
    @Override
    protected void updateItem(CvikPolozka cvik, boolean empty) {
        super.updateItem(cvik, empty);
        if (empty) {
            setText(null);
            setGraphic(null);
        } else {
            try {
                setText(Core.vratHodnotu("cvik", "id_cvik", "nazev", String.valueOf(cvik.getId())));
            } catch (SQLException e) {
                throw new RuntimeException(e);
            }
            Button var = new Button();
            var.setUserData(cvik.getId());
            ImageView img = new ImageView();
            img.setImage(CvikyController.obrazky[0]);
            img.setFitHeight(10);
            img.setFitWidth(10);
            var.setGraphic(img);
            var.setOnAction(e -> {
                try {
                    Core.upravHodnotuPodle2Sloupcu("oblibeny_cvik","id_uzivatele",String.valueOf(Core.getUserID()),"id_cviku",String.valueOf(cvik.getId()),"je_oblibeny","0");
                } catch (SQLException ex) {
                    throw new RuntimeException(ex);
                }
            });
            setGraphic(var);
        }
    }
}
