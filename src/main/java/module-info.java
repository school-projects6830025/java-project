module cz.vse.fitness_buddy {
    requires javafx.controls;
    requires javafx.fxml;
    requires javafx.web;
    requires java.sql;


    opens cz.vse.fitness_buddy.main to javafx.fxml;
    exports cz.vse.fitness_buddy.main;
    exports cz.vse.fitness_buddy.coreFunc;
    opens cz.vse.fitness_buddy.coreFunc to javafx.fxml;
    exports cz.vse.fitness_buddy.main.listCells;
    opens cz.vse.fitness_buddy.main.listCells to javafx.fxml;
}