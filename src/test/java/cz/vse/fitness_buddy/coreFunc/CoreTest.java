package cz.vse.fitness_buddy.coreFunc;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import java.sql.SQLException;

import static org.junit.Assert.*;

public class CoreTest {
    static Core core;

    @BeforeClass
    public static void setup() {
        core = new Core();
    }

    @AfterClass
    public static void teardown() {
        core = null;
    }

    @Test
    public void testVlozUzivatele() {
        String jmeno = "testUser";
        String email = "test@test.com";
        String heslo = "password";
        int expectedUserId = -1;

        int actualUserId = -1;
        try {
            actualUserId = core.vlozUzivatele(jmeno, email, heslo);
        } catch (SQLException e) {
            fail("Unexpected SQLException was thrown.");
        }

        assertNotEquals(expectedUserId, actualUserId);
    }

    @Test
    public void testUpravHodnotu() {
        String jmenoTabulky = "registrovany_uzivatel";
        String jmenoSloupceRidici = "id_uziv";
        String hodnota = "1";
        String jmenoSloupceKeZmene = "email";
        String novaHodnota = "admin@testovany.cz";

        try {
            core.upravHodnotu(jmenoTabulky, jmenoSloupceRidici, hodnota, jmenoSloupceKeZmene, novaHodnota);
        } catch (SQLException e) {
            fail("Unexpected SQLException was thrown.");
        }
    }

    @Test
    public void testExistujeZaznam() {
        String jmenoTabulky = "registrovany_uzivatel";
        String jmenoSloupce = "id_uziv";
        String hodnota = "1";

        boolean result = false;

        try {
            result = core.existujeZaznam(jmenoTabulky, jmenoSloupce, hodnota);
        } catch (SQLException e) {
            fail("Unexpected SQLException was thrown.");
        }

        assertTrue(result);
    }

    @Test
    public void testVratHodnotu() {
        String jmenoTabulky = "registrovany_uzivatel";
        String jmenoSloupceRidici = "id_uziv";
        String jmenoSloupceZiskany = "email";
        String hodnota = "1";

        try {
            String result = core.vratHodnotu(jmenoTabulky, jmenoSloupceRidici, jmenoSloupceZiskany, hodnota);
        } catch (SQLException e) {
            fail("Unexpected SQLException was thrown.");
        }
    }

    @Test
    public void testUpravHodnotuPodle2Sloupcu() {
        String jmenoTabulky = "registrovany_uzivatel";
        String jmenoSloupceRidici1 = "id_uziv";
        String hodnota1 = "1";
        String jmenoSloupceRidici2 = "heslo";
        String hodnota2 = "admin";
        String jmenoSloupceKeZmene = "uzivatelske_jmeno";
        String hodnotaKUlozeni = "admin";

        try {
            core.upravHodnotuPodle2Sloupcu(jmenoTabulky, jmenoSloupceRidici1, hodnota1, jmenoSloupceRidici2, hodnota2, jmenoSloupceKeZmene, hodnotaKUlozeni);
        } catch (SQLException e) {
            fail("Unexpected SQLException was thrown.");
        }
    }

    @Test
    public void testVratHodnotuPodle2Sloupcu() {
        String jmenoTabulky = "registrovany_uzivatel";
        String jmenoSloupceRidici1 = "id_uziv";
        String hodnota1 = "1";
        String jmenoSloupceRidici2 = "heslo";
        String hodnota2 = "admin";
        String jmenoSloupceZiskany = "email";

        try {
            String result = core.vratHodnotuPodle2Sloupcu(jmenoTabulky, jmenoSloupceRidici1, hodnota1, jmenoSloupceRidici2, hodnota2, jmenoSloupceZiskany);
        } catch (SQLException e) {
            fail("Unexpected SQLException was thrown.");
        }
    }

    @Test
    public void testVytvorOblibenyCvikZaznamy() {
        int id = 1;

        try {
            core.vytvorOblibenyCvikZaznamy(id);
        } catch (SQLException e) {
            fail("Unexpected SQLException was thrown.");
        }
    }
}
